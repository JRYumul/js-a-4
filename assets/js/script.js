let hideitem = document.getElementsByClassName("hide")
let lgitem = document.getElementsByClassName("list-group-item")
let delitem = document.getElementsByClassName("far")

function hider(){
    $(this).children().first().stop(true,true).animate({width:'toggle'}, 500);
}

function toggleOn(){
    $(this).css("text-decoration", "line-through");
    $(this).off("click", toggleOn)
    $(this).on("click", toggleOff)
}

function toggleOff(){
    $(this).css("text-decoration", "none");
    $(this).off("click", toggleOff)
    $(this).on("click", toggleOn)
}

function fader(){
    $(this).parentsUntil("ul").fadeOut(1000, function(){
        $(this).parentsUntil("ul").remove();
    })
}

$(".fas").on("click", function(){
    $("input[type='text']").slideToggle(500, function(){
        $("input[type='text']").focus()
    })
})

$(hideitem).on("mouseenter", hider)
$(hideitem).on("mouseleave", hider)

$(lgitem).on("click", toggleOn)

$(delitem).on("click", fader)

$("input[type='text']").keypress(function(e){
    if(e.which === 13){
        let listtext = $(this).val()
        let lgnum = lgitem.length
        let hidenum = hideitem.length
        let delnum = delitem.length
        $(this).val("")
        $("ul").append('<div class="row hide"><div class="bg-danger text-white text-center trash" style="display:none"><i class="far fa-trash-alt"></i></div><li class="col list-group-item">' + listtext + '</li></div>')
        $(hideitem[hidenum]).on("mouseenter", hider)
        $(hideitem[hidenum]).on("mouseleave", hider)
        $(lgitem[lgnum]).on("click", toggleOn)
        $(delitem[delnum]).on("click", fader)
    }
})